# PCMT CIS Bundle for Akeneo Pim

## Key features

CIS Bundle provides CIS features for PCMT.

## Installation

To install the bundle, you need to manually add some rows in composer.json file.

```
// ...
"repositories": [

    // ...
    
    {
        "type": "vcs",
        "url": "https://gitlab.com/pcmt/cis-bundle.git",
        "branch": "master",
        "no-api": true
    },
    
    // ...
    
],
"require": {

    // ...
    
    "pcmt/cis-bundle": "dev-master@dev",
    
    // ...
    
},

//...
```

After that simply run `composer update` command.

### Enable the bundle
Enable the bundle in the kernel:

```php
<?php
// config/bundles.php

return [
    // ...
    PcmtCISBundle\PcmtCISBundle::class => ['all' => true],
];
```

## Development
### Running Test-Suits
The PcmtCISBundle is covered with tests and every change and addition has also to be covered with unit tests. It uses PHPUnit.

To run the tests you have to change to this project's root directory and run the following commands in your console:

```
make unit
```

### Coding style
PcmtCISBundle the coding style can be checked with Easy Coding Standard.

```
make ecs
```
