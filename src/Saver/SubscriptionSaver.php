<?php

declare(strict_types=1);

/*
 * Copyright (c) 2020, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

namespace PcmtCISBundle\Saver;

use Akeneo\Tool\Component\StorageUtils\Saver\SaverInterface;
use Akeneo\Tool\Component\StorageUtils\StorageEvents;
use Doctrine\Persistence\ObjectManager;
use PcmtCISBundle\Entity\Subscription;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class SubscriptionSaver implements SaverInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(
        ObjectManager $objectManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->objectManager = $objectManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function save($subscription, array $options = []): void
    {
        $this->validateSubscription($subscription);

        $options['unitary'] = true;

        $this->eventDispatcher->dispatch(new GenericEvent($subscription, $options), StorageEvents::PRE_SAVE);

        $this->objectManager->persist($subscription);

        $this->objectManager->flush();

        $this->eventDispatcher->dispatch(new GenericEvent($subscription, $options), StorageEvents::POST_SAVE);
    }

    protected function validateSubscription(object $subscription): void
    {
        if (! $subscription instanceof Subscription) {
            throw new \InvalidArgumentException(
                sprintf('Expects a "%s", "%s" provided.', Subscription::class, get_class($subscription))
            );
        }
    }
}
